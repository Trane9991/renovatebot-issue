module bitbucket.org/trane9991/renovatebot-issue

go 1.15

require (
	bitbucket.org/AiSee/common-go-lib v0.0.0-20191112142628-3dc422aaa32a
	bitbucket.org/DarkBoss/go-release-notes v0.0.0-20190302140415-7b58f39845b0
	bitbucket.org/trane9991/fake v0.0.1
	github.com/Microsoft/go-winio v0.4.16 // indirect
	github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351 // indirect
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/xanzy/ssh-agent v0.3.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
)

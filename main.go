package main

import (
	"fmt"

	common "bitbucket.org/AiSee/common-go-lib"
	"bitbucket.org/DarkBoss/go-release-notes/errors"
	"bitbucket.org/trane9991/fake"
)

func main() {
	m := map[string]string{
		"a": "b",
	}

	errors.Print(fmt.Errorf(fake.Hello()))
	fmt.Println(common.StringInMap("a", m))

}
